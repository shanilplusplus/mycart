import {Products} from "../../../data.js";

export default async (req, res) => {
    res.status(200).json(Products);
}