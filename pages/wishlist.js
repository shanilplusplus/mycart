import React, {useState} from 'react';
import Nav from "../src/components/Navigation";
import {useSelector, useDispatch} from "react-redux";
import { useDisclosure } from "@chakra-ui/react";
import absoluteUrl from 'next-absolute-url'

  import Product from "../src/components/Product";

const WishList = ({data}) => {
    const Likes = useSelector(state => state.hearts);

    const btnRef = React.useRef()
    const { isOpen, onOpen, onClose } = useDisclosure();
     
    const filteredData = data.filter( e => {
        return Likes.some( f => {
            return f == e.id
        })
    })
    

    return(
        <div>
                <Nav />
                <div className="product_list">
                {
                    filteredData.map(e => {
                        return(

                            <Product key={e.id} id={e.id} title={e.title} image={e.image} price={e.price} />
                        )
                    })
                }

            </div>
        </div>
    )
}

export async function getServerSideProps({ req}){

    const { origin } = absoluteUrl(req);


    // const gigaurl =
    // process.env.NODE_ENV === "development"
    // ? "http://localhost:3000"
    // : "https://websitename.vercel.app";

const response = await fetch(`${origin}/api/products`);
const data = await response.json(); 

return{
    props:{
    data
    }
}


}

export default WishList;