// import Head from 'next/head';
import Image from 'next/image';
import SideDrawer from "../src/components/Drawer";

import Product from "../src/components/Product";
import Nav from "../src/components/Navigation";
import absoluteUrl from 'next-absolute-url'

export default function Home({data}) {
  return (
    <div >
      
    
      <Nav />

      <div className="product_list">
          {Object.entries(data).map(([index, {title, image, price, id}]) => {
                
                  
                return(
                  <Product key={id} id={id} title={title} image={image} price={price} />
                  
                  
                )
            })}
      </div>
      
    </div>
  )
}



export async function getServerSideProps({ctx, req}){

  const { origin } = absoluteUrl(req);

  // const gigaurl =
  // process.env.NODE_ENV === "development"
  //   ? "http://localhost:3000"
  //   : "https://websitename.vercel.app";

  const response = await fetch(`${origin}/api/products`);
  const data = await response.json(); 

  return{
    props:{
      data
    }
  }


}