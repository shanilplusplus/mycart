// import Link from 'next/link';
import {useSelector, useDispatch} from "react-redux";
// import { Badge } from "@chakra-ui/react"
import Checkout from "../src/components/Checkout";
import Nav from "../src/components/Navigation";
import ShoppingCart from "../src/components/Cart";


const cart = () => {

    return(
    <>

        <Nav />
        <ShoppingCart />
        <Checkout />

    </>
    )
}

export default cart;