import '../styles/globals.css';
import '../styles/main.scss';
import {wrapper} from '../src/store/configureStore';
import { ChakraProvider, theme, CSSReset  } from "@chakra-ui/react";

function MyApp({ Component, pageProps }) {
  return(
    
  <ChakraProvider theme={theme}>
    <CSSReset />
    <Component {...pageProps} />
  </ChakraProvider> 

  )
}

export default wrapper.withRedux(MyApp);
