# Twitr

Shopping Cart using React.js and Redux with Persistance

### Prerequisites

you need to have node installed in your computer.


## Getting Started

to get started on you local machine, clone this repo, and install all the node modules. Then bundle a production ready copy using webpack. Finally, fire up the node server.


### Installing
cd into the folder
install npm dependencies


### Running Locally

```
npm install
npm run dev
```

### View Demo
[myCart]https://mycart-mu.vercel.app/)

## Built With

* [React.js](https://reactjs.org/) - for React and ReactDom libraries
* [Next.js](https://nextjs.org/) - SSR
* [Redux](https://redux.js.org/) - State Management Library
* [babel](https://babeljs.io/) - compiling and transpling
* [sass](https://sass-lang.com/) - for scss



## Authors

* **Shaun Sigera** (http://sigera.ca/)


## License

This project is licensed under the MIT License


