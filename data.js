export const Products = [
    {id: 0,
        title: "Source Naturals, Broccoli Sprouts",
        price: 19.99,
        description:"A number of studies have shown that a diet rich in cruciferous vegetables such as broccoli, Brussels sprouts, cabbage and cauliflower can be beneficial to your health. Researchers have isolated a key component of broccoli called sulforaphane, which may have many protective properties. Sulforaphane, an isothiocyanate is believed to stimulate enzymes in the body. Freshly germinated broccoli sprouts contain up to 50 times the concentration of isothiocyanates as mature broccoli. Source Naturals Broccoli Sprouts Extract provides 2,000 mcg of sulforaphane per serving, up to 5 times more than an average serving of fresh broccoli.",
        image: "https://s3.images-iherb.com/sns/sns01104/l/12.jpg"
    },
    {
        id: 1,
        title: "Y.S. Eco Bee Farms, Royal Jelly, Bee Pollen, Propolis",
        price: 29.99,
        description:"This synergistic combination of super whole foods is a mega nutritional complex containing all the vital nutrients naturally occurring for life forming, readily absorbed and utilized by the body.",
        image: "https://s3.images-iherb.com/yso/yso74734/w/11.jpg"
    },
    {
        id: 2,
        title: "Kettle Foods, Potato Chips, Honey Dijon",
        price: 2.99,
        description:"Guaranteed fresh and delicious through the 'Best Before' date. Please note this date and the UPC code in all correspondence regarding this product.",
        image: "https://s3.images-iherb.com/ktt/ktt03070/w/6.jpg"
    },
    {
        id: 3,
        title: "Sir Kensington's, Avocado Oil Mayonnaise",
        price: 9.99,
        description:"Avocado oil, organic certified humane free range egg yolks, water, distilled vinegar, salt, lime juice concentrate, citric acid, lime oil.",
        image: "https://s3.images-iherb.com/sir/sir00537/w/40.jpg"
    },
    {
        id: 4,
        title: "Nutiva, Organic Coconut Sugar, Unrefined",
        price: 6.99,
        description:"Nutiva® Organic Coconut Sugar is made from fresh coconut tree sap collected from cut flower buds. Excellent for baking or in beverages like coffee and tea, it has a subtly sweet taste that is similar to brown sugar with a hint of caramel. Even better, it has a lower glycemic index than cane sugar and is organic and non-GMO. Enjoy in all your favorite recipes!",
        image: "https://s3.images-iherb.com/nut/nut10301/w/19.jpg"
    },
    {
        id: 5,
        title: "Twinings, Herbal Tea, Pure Peppermint, Caffeine Free, 50 Tea Bags",
        price: 7.99,
        description:"In 1706, Thomas Twining began selling fine tea from an English storefront in The Strand, London. Today, Twinings still sells some of the world's best teas from the original store and in more than 100 countries throughout the world.",
        image: "https://s3.images-iherb.com/twn/twn22503/w/16.jpg"
    },
    {
        id: 6,
        title: "Celtic Sea Salt, Light Grey Celtic",
        price: 17.99,
        description:"Celtic Sea Salt®  enhances the taste of any dish while adding essential nutrients to your diet, with zero additives. Our sustainable harvesting methods combine tradition with innovation designed to preserve the vital balance of ocean's minerals.",
        image: "https://s3.images-iherb.com/css/css10008/w/10.jpg"
    },
    {
        id: 7,
        title: "Nutiva, Organic Coconut Oil, Virgin",
        price: 12.49,
        description:"This creamy taste of the tropics is great for sautéing, baking, and enhancing your favorite recipes. Savor the rich aroma and enticing light taste of this cold-pressed, and unrefined oil.",
        image: "https://s3.images-iherb.com/nut/nut20005/w/46.jpg"
    },
    {
        id: 8,
        title: "Yogi Tea, Positive Energy, Sweet Tangerine",
        price: 4.99,
        description:"Sweet Tangerine Positive Energy tea combines herbs traditionally used to uplift the spirit and a blend of Assam Black Tea and Yerba Mate to help provide energy. Lotus Flower and Tangerine flavors add sweet, floral and fruity notes for a bright and spirited tea that's sure to leave you smiling.",
        image: "https://s3.images-iherb.com/ygt/ygt20455/w/36.jpg"
    },
    {
        id: 9,
        title: "Now Foods, Organic Coconut Flour",
        price: 7.00,
        description:"With Organic Coconut Flour you'll use far less than grain-based flours, approximately 1/4 to 1/3 the amount of regular flour called for. You'll also need to increase the amount of liquid used, which can be done by increasing the amount of eggs or egg-substitute. With its unique characteristics we recommend using recipes specifically developed for Coconut Flour.",
        image: "https://s3.images-iherb.com/now/now06917/w/3.jpg"
    },
    {
        id: 10,
        title: "Primal Kitchen, Avocado Oil, 16.9 fl oz",
        price: 19.99,
        description:"Avocado oil.",
        image: "https://s3.images-iherb.com/pmk/pmk00019/w/15.jpg"
    },
    {
        id: 11,
        title: "Primal Kitchen, Organic Ketchup, Unsweetened",
        price: 2.99,
        description:"Organic tomato concentrate, organic balsamic vinegar, less than 2% of salt, organic onion powder, organic garlic powder, organic spices.",
        image: "https://s3.images-iherb.com/pmk/pmk00712/w/8.jpg"
    },
    {
        id: 12,
        title: "Coconut Secret, Raw Coconut Vinegar, 12.7 fl oz",
        price: 7.49,
        description:"Organic coconut sap naturally aged for 8 months to one year.",
        image: "https://s3.images-iherb.com/ccs/ccs00203/w/3.jpg"
    },
    {
        id: 13,
        title: "Eden Foods, Organic Sesame Oil, Unrefined, 16 fl oz",
        price: 4.49,
        description:"Eden Organic Sesame Oil is ideal for all sauteing, in baking, dressings, and sauces. It is simply pressed from Eden select seed and lightly filtered retaining sesame's full aroma and flavor. Contains the revered antioxidants sesamol and sesamin. Nitrogen flushed when bottled.",
        image: "https://s3.images-iherb.com/edn/edn00025/w/12.jpg"
    },
    {
        id: 14,
        title: "Spectrum Culinary, Organic Extra Virgin Olive Oil,",
        price: 11.49,
        description:"Spectrum Culinary, Organic Extra Virgin Olive Oil.",
        image: "https://s3.images-iherb.com/spt/spt42137/w/6.jpg"
    },
    {
        id: 15,
        title: "Walden Farms, French Dressing",
        price: 2.49,
        description:"Just the right touch of the world’s finest aged vinegars, fresh ground herbs and spices, triple filtered water and natural flavors makes Walden Farms French Salad Dressing delicious and perfect when trying to eat right.",
        image: "https://s3.images-iherb.com/wal/wal33114/w/1.jpg"
    },
    {
        id: 16,
        title: "Wholesome, Spreadable Organic Raw Unfiltered Honey",
        price: 22.49,
        description:"We go the extra mile to ensure our organic honey is mindfully delicious, as we believe healthy bees make the best tasting honey. Unlike some local honeys, we keep ours free from harmful pesticides and antibiotics to give you a purely wholesome choice.",
        image: "https://s3.images-iherb.com/whs/whs89165/w/14.jpg"
    },
    {
        id: 17,
        title: "General Mills, Honey Nut Cheerios",
        price: 5.49,
        description:"Made with real honey, so each bowl is really delicious.",
        image: "https://s3.images-iherb.com/gem/gem12479/w/10.jpg"
    },
    {
        id: 18,
        title: "Y.S. Eco Bee Farms, Raw Honey",
        price: 15.49,
        description:"Unprocessed, raw state provides maximum level of natural antioxidants and full beneficial factors as a functional whole food.",
        image: "https://s3.images-iherb.com/yso/yso12112/w/11.jpg"
    },
    {
        id: 19,
        title: "Nature's Path, Organic Heritage Flakes Cereal",
        price: 8.29,
        description:" Heritage Flakes cereal we baked together organic ancient grains like Kamut khorasan wheat, oats, spelt, and quinoa with honey for a super crunchy, fiber-rich breakfast you can't help but love. And with such simple, wholesome ingredients - you'll see how good organic food can be.",
        image: "https://s3.images-iherb.com/npa/npa77020/w/17.jpg"
    },
    {
        id: 20,
        title: "Nescafé, Taster's Choice, Instant Coffee, French Roast",
        price: 7.79,
        description:" Nescafe Taster's Choice French Roast is our darkest roast yet made from 100% pure responsibly sourced coffee beans. Master Coffee Crafters roast and brew our special blend of premium quality coffee beans. They then flash freeze the coffee to lock in the full-bodied cup of intense, bold flavors with smoky overtones. Enjoy a deliciously invigorating cup of well-crafted coffee made simple.",
        image: "https://s3.images-iherb.com/ncf/ncf55360/w/1.jpg"
    }

]