const AddProduct = ( id = 0,title = 'undefined', image = 'https://via.placeholder.com/150') => ({
    type: 'ADD_ITEM',
    id,
    title,
    image
})


const RemoveProduct = (id = 0) => ({
    type: 'REMOVE_ITEM',
    id
});


export {AddProduct,RemoveProduct}
