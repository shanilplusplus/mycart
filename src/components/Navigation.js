import { useState } from 'react';
import {
 
    Flex,
    Button,
    IconButton,
    Heading,
    ButtonGroup,

} from '@chakra-ui/react'
import { HamburgerIcon, CloseIcon } from '@chakra-ui/icons'
import NextLink from 'next/link';
import { FiShoppingCart } from 'react-icons/fi';
import {useSelector, useDispatch} from "react-redux";
import Image from 'next/image';
import { AiFillHeart } from 'react-icons/ai';
import Head from 'next/head';



const Nav = () => {

    const Products = useSelector(state => state.products);
    const Shipping = useSelector(state => state.shipping);
    const dispatch = useDispatch();

    console.log(Shipping, "shipping");

    const [display, changeDisplay] = useState('none')


const cartQty = () => {
let sum = Products.reduce((sum, p) => sum + p.qty*1, 0);
return sum;

}

const hello = (e) => {
console.log(e.target.value)

dispatch({
    type: 'CHANGE_SHIPPING',
    province: e.target.value
})
}
    return(
        <>
        <Head>
        <title>myCart</title>
        <meta name="description" content="Itesm in Cart" />
        <link rel="icon" href="/favicon.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Head>
        <Flex className="navnav" mt={5} >
        <Flex align="center" mr={5} mt={4} mb={2 } ml={5} position="left" 
        > 
        <Heading as="h1" size="lg" letterSpacing={"tighter"}>
        <NextLink  href="/" passHref>
        myCart
        </NextLink>
    </Heading>
    </Flex>
    {/* Desktop */}
    <Flex
        
        display={['none', 'none', 'flex','flex']}
        
    >
    
    <ButtonGroup size="sm" isAttached variant="outline"  >
    
    
    <p className="shipping_to">   {Shipping.map((e,i) =>  {
        if(e.Selected && e.Province){
        return(
            <Image
            src={`/${e.Province}.ico`}
            alt="Picture of the author"
            width={30}
            height={20}
            key={i}
                />
        )
        }
    })}<span className="shipping_caption">Shipping To</span> </p>
            <select  id="province" onChange={hello}>

            {Shipping.map((e,i) => {
                return(
                <option  defaultValue={e.Province} selected={e.Selected && e.Province} key={i}>
                    
                    
                    {e.Province}</option>
                )
                })}

                
                </select>

        </ButtonGroup>
    

        <Flex 
        align="right" position="right"
    >
        <NextLink href="/wishlist" passHref>
        <Button
            as="a"
            variant="ghost"
            aria-label="wishlist"
            my={5}
            w="100%"
            leftIcon={<AiFillHeart />}
            ml={5}
            mr={5}
        >
            WishList
                </Button>
        </NextLink>
        
        <NextLink  href="/cart" passHref>
        <Button mt={5}
        as="a"
        variant="ghost"
        aria-label="Contact"
        my={5}
        w="100%"
        
        >
    
        <FiShoppingCart  size={22} />
        
        <span className='badge badge-warning' id='lblCartCount4'> {(Products.length < 0) ? 0 : cartQty()}  </span>
        </Button>
        </NextLink>
        </Flex>

    </Flex>

    {/* Mobile */}
    <IconButton
        aria-label="Open Menu"
        size="lg"
        mr={2}
        icon={
        <HamburgerIcon />
        }
        onClick={() => changeDisplay('flex')}
        display={['flex', 'flex', 'none', 'none']}
    />
    
    {/* </Flex> */}

    {/* Mobile Content */}
    <Flex
        w='100vw'
        display={display}
        bgColor="gray.50"
        zIndex={20}
        h="100vh"
        pos="fixed"
        top="0"
        left="0"
        overflowY="auto"
        flexDir="column"
    >
    <Flex justify="flex-end">
        <IconButton
        mt={2}
        mr={2}
        aria-label="Open Menu"
        size="lg"
        icon={
            <CloseIcon />
        }
        onClick={() => changeDisplay('none')}
        />
    </Flex>

    <Flex
        flexDir="column"
        align="center"
    >
        <p className="shipping_to">   {Shipping.map((e,i) =>  {
        if(e.Selected && e.Province){
        return(
            <Image
            src={`/${e.Province}.ico`}
            alt="Picture of the author"
            width={30}
            height={20}
            key={i}
                />
        )
        }
    })}<span className="shipping_caption">Shipping To</span> </p>
            <select  id="province" onChange={hello}>

            {Shipping.map((e, i) => {
                return(
                <option  defaultValue={e.Province} selected={e.Selected && e.Province} key={i}>
                    
                    
                    {e.Province}</option>
                )
                })}

                
                </select>
        

        <NextLink href="/wishlist" passHref>
        <Button
            as="a"
            variant="ghost"
            aria-label="wishlist"
            my={5}
            w="100%"
            leftIcon={<AiFillHeart />}
            ml={5}
            mr={5}
        >
            WishList
                </Button>

        
        </NextLink>
        <NextLink  href="/cart" passHref>
        <Button
        mt={5}
        as="a"
        variant="ghost"
        aria-label="cart"
        my={5}
        w="100%"
        
        >
    
        <FiShoppingCart  size={22}/>
        
        <span className='badge badge-warning' id='lblCartCount4'> {(Products.length < 0) ? 0 : cartQty()}  </span>
        </Button>
        </NextLink>


    </Flex>
    </Flex>
    </Flex>
    </>
    )
}


export default Nav;