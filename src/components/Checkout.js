import {useSelector, useDispatch} from "react-redux";


const Checkout = () => {

    const Products = useSelector(state => state.products);
    const Shipping = useSelector(state => state.shipping);
  const dispatch = useDispatch();
    const subTotal = Products.reduce((acc, obj)=> {
        return acc + (obj.qty * obj.price)
    }, 0)

  
    const ll = Shipping.findIndex(o => o.Selected === true)
    const shippingCost = Shipping[ll].Cost;
    
    const total = subTotal + shippingCost
    console.log(shippingCost, 'shippingCost')
    return(
    <div className="checkout">
       
        <h2>Order Summary</h2>
        

        <p>Order SubTotal {subTotal.toFixed(2)}</p>
        
        <p>Shipping: {subTotal ? shippingCost : '--'}</p>

        <p>Total : {subTotal ? total.toFixed(2) : '--'}</p>
        
    </div>
    )
}

export default Checkout;