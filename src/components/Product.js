
import Image from 'next/image';
import React, {useState} from 'react';
import { AiFillHeart,AiOutlineHeart } from 'react-icons/ai';
import {useSelector, useDispatch} from "react-redux";

import { FaCartPlus,FaHeart } from 'react-icons/fa';

// import SideMenu from "../components/SideMenu";
import { useDisclosure } from "@chakra-ui/react";
import { Button} from "@chakra-ui/react";
import ShoppingCart from "../components/Cart";
import { useToast } from "@chakra-ui/react";
import Link from 'next/link';
import {
    Drawer,
    DrawerBody,
    DrawerFooter,
    DrawerHeader,
    DrawerOverlay,
    DrawerContent,
    DrawerCloseButton,
  } from "@chakra-ui/react";




const Product = ({id, title, image, price}) => {
    
    
    
    const { isOpen, onOpen, onClose } = useDisclosure();
    const dispatch = useDispatch()
    const Products = useSelector(state => state.products);
    const Hearts = useSelector(state => state.hearts);
 

    // console.log(Hearts, "Hearts");
    let hasItems = Products.length;
    const toast = useToast();
    let existCart = Products.findIndex(o => o.id === id);
    // let existHeart = Hearts.findIndex(o => o === id);

    // console.log(existHeart, "existHeart");
 


    
    const btnRef = React.useRef()
    // let data = Object.assign(title, image, id)
    // const product = {
    //     title : data.title,
    //     id : data.id,
    //     image : data.image
    // }

    const AddToCart = (id, title, image, price) => {
        // let product = {id: title, image}
        
        //  product = Object.assign(data,{qty: 0} );
        // console.log(product)
        
        

        //    {hasItems && (
        //         Products.map(prod => {
        //             if(prod.id === data.id){
        //                 console.log('update')
        //             } else {
        //                 dispatch({
        //                     type: 'ADD_ITEM',
        //                     product
        //                 })
        //             }
        //         })
        //    )}

        //    {!hasItems &&(
        //     dispatch({
        //         type: 'ADD_ITEM',
        //         product
        //     })
        //    )}
        // () => {onOpen()}

       

        dispatch({
            type: 'ADD_ITEM',
            id, title, image, price
        });
        
       
        onOpen();

            

    }

    const existHeart = () => {
        const pp = Hearts.findIndex(e => e === id)
        console.log(pp, "pp")
        if(pp < 0){
            return <AiOutlineHeart />
        } else {
            return <AiFillHeart />
        }
    }
    
    return(
        
            <div className="product" key={id}>
                


                    <p onClick={() => {
                        dispatch({
                        type: 'LIKE_PRODUCT',
                        id
                    })
                    }}>{existHeart()}</p>

                    <br />
                
                    <Image
                        width={134}
                        height={175}
                        src={image}
                        />
                
                        <br />
                        <p>{title}</p>
                    <p>{price}</p>
              
                    <Button 
                    ref={btnRef}
                    
                    colorScheme="teal"
                    variant="outline"
                    leftIcon={<FaCartPlus />}
                    onClick={() => AddToCart(id, title, image, price)
                    }
                    >Add To Cart</Button>
                    
                    

<Drawer
    isOpen={isOpen}
    placement="right"
    onClose={onClose}
    finalFocusRef={btnRef}
    >
    <DrawerOverlay />
    <DrawerContent>
        <DrawerCloseButton />
        <DrawerHeader>Your Shopping Cart</DrawerHeader>

        <DrawerBody>
        <ShoppingCart />
        </DrawerBody>

        <DrawerFooter>
        <Button variant="outline" mr={3} onClick={onClose}>
            Continue Shopping
        </Button>
        <Button colorScheme="blue">
        <Link  href="/cart" passHref>
                Checkout
            </Link>

        </Button>
        </DrawerFooter>
    </DrawerContent>
    </Drawer>
                    
                    </div>

                    
    
    )
}

export default Product;