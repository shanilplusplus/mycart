import {useSelector, useDispatch} from "react-redux";
import { Badge } from "@chakra-ui/react";
import Image from 'next/image';
import { Button, ButtonGroup } from "@chakra-ui/react"


const ShoppingCart = () => {
    const Products = useSelector(state => state.products);
 
  const dispatch = useDispatch()
    return(
        <>
            <h2 className="cart-page-title">Your Shopping Cart</h2>

            <div className="shopping-cart">
                {Products && Products.map(e => {
                return(
                    <div key={e.id} className="shopping-cart-item">
                        <div className="shopping-cart-item__image">
                         <Image
                            width={140}
                            height={120}
                            src={e.image}
                            />
                        </div>

                        <div className="shopping-cart-item__description">
                           

                        <p className="shopping-cart-item__description__name">{e.title}</p>
                        
                        
                        <p  className="shopping-cart-item__description__price">${e.price}</p>
                        
                        </div>
                        <div className="shopping-cart-item__qty">
                            <div className="shopping-cart-item__qty__plus">
                                {e.qty < 10 ? (<button  onClick={() =>dispatch({
                                        type: 'ADD_ITEM',
                                        id: e.id,
                                        title: e.title,
                                        image: e.image
                                    })}>+</button>) : (<Button className="button__disabled" disabled>+</Button>)}
                            </div>
                           <div className="shopping-cart-item__qty__qty">
                                <p>{e.qty}</p>
                            </div> 
                        <div className="shopping-cart-item__qty__minus">
                        {e.qty >= 1 && (
                            <p onClick={() => dispatch({
                            type: 'REMOVE_ITEM',
                            id : e.id
                            })}>-</p>
                        )} 
                        </div>

                        
                        <div className="shopping-cart-item__qty__removebutton">
                            
                        <Button colorScheme="red" size="xs" onClick={() => dispatch({
                            type: 'REMOVE_PRODUCT',
                            id : e.id
                        })}>Remove</Button>
                        <span>{e.qty == 10 && ( <Badge variant="outline" colorScheme="red">
                            Maximum 10
                                </Badge>)}</span>
                        </div>
                        
                            
                            

                        </div>
                        <p className="shopping-cart-item__total">   ${(e.qty * e.price).toFixed(2)}</p>
                        

                    </div>
                )
            })}
            <h3 className="shopping_cart_subtotal">SubTotal:   $
                                {Products.reduce(function(acc, obj){
                                    return acc + (obj.qty * obj.price)
                                }, 0).toFixed(2)}
                            </h3>
            </div>

        </> 

    )
}


export default ShoppingCart; 


// <Badge variant="outline" colorScheme="red">
//                                     Maximum quantity allowed is 10
//                                 </Badge>