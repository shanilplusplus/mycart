import ShoppingCart from "../components/Cart";
import { Button} from "@chakra-ui/react";
import {
    Drawer,
    DrawerBody,
    DrawerFooter,
    DrawerHeader,
    DrawerOverlay,
    DrawerContent,
    DrawerCloseButton,
} from "@chakra-ui/react";


const SideDrawer = () => {
    return(
        <Drawer
        isOpen={isOpen}
        placement="right"
        onClose={onClose}
        finalFocusRef={btnRef}
    >
    <DrawerOverlay />
    <DrawerContent>
        <DrawerCloseButton />
        <DrawerHeader>Your Shopping Cart</DrawerHeader>

        <DrawerBody>
        <ShoppingCart />
        </DrawerBody>

        <DrawerFooter>
        <Button variant="outline" mr={3} onClick={onClose}>
            Continue Shopping
        </Button>
        <Button colorScheme="blue">Checkout</Button>
        </DrawerFooter>
    </DrawerContent>
    </Drawer>
    )
}


export default SideDrawer;