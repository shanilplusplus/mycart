import produce from "immer";

const ShippingReducerDefaultState = [
    {
        Province : "quebec",
        Cost : 9.99,
        Selected: true
    },
    {
        Province : "ontario",
        Cost : 19.99,
        Selected: false
    },
    {
        Province : "alberta",
        Cost : 99.99,
        Selected: false
    }
]

const ShippingReducer = produce((draft, action = {}) => {
    switch(action.type){
        case 'CHANGE_SHIPPING':
            console.log(action.province)
            let alreadyExist = draft.findIndex(o => o.Selected === true)
            
            let exist = draft.findIndex(o => o.Province === action.province);
            draft[alreadyExist].Selected = !draft[alreadyExist].Selected
            draft[exist].Selected = !draft[exist].Selected
                return draft;
        default:
                return draft;
    }

}, ShippingReducerDefaultState)


export default ShippingReducer;