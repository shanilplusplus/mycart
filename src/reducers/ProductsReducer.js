import produce from "immer";


const ProductsReducerDefaultState = {
    Products: []
};

const ProductsReducer = produce((draft, action = {}) => {
    let exist = draft.findIndex(o => o.id === action.id);
    switch(action.type) {
        
        case 'ADD_ITEM':
            if(exist >= 0){
                if(draft[exist].qty < 10){
                    draft[exist].qty += 1
                }
                
            } else {
                return [...draft, {
                    title : action.title, 
                    id : action.id,
                    price: action.price,
                    image: action.image,
                    qty: 1
                }]      
            }
            break
            case 'REMOVE_ITEM':
                
                if(draft[exist].qty <= 1){
                    draft.splice(exist, 1)
                } else {
                    draft[exist].qty -= 1;
                }
                break
            case 'REMOVE_PRODUCT':
                draft.splice(exist, 1)
                break
            default:
                return draft;
        

    }
       


},ProductsReducerDefaultState.Products)

export default ProductsReducer;