import produce from "immer";

const LikedProductsReducerDefaultState = ["e"];

const LikedProductsReducer = produce((draft, action = {}) => {
    let exist = draft.findIndex(o => o === action.id);
    
    switch(action.type){
        case 'LIKE_PRODUCT':

            if(exist > -1){
                draft.splice(exist, 1);
            } else{
                draft.push(action.id);
            }
        default:
            return draft;
    }
},LikedProductsReducerDefaultState)

export default LikedProductsReducer;