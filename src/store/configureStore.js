import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import {createWrapper} from 'next-redux-wrapper';
import FormStateReducer from "../reducers/FormStateReducer";
import ProductsReducer from "../reducers/ProductsReducer";
import ShippingReducer from "../reducers/ShippingReducer";
import LikedProductsReducer from "../reducers/LikedProductsReducer";
import { composeWithDevTools } from 'redux-devtools-extension';
import { persistStore } from 'redux-persist';

let store;

const isClient = typeof window !== 'undefined';




const configureStore = () => {

    if(isClient){
        const { persistReducer } = require('redux-persist');
        const storage = require('redux-persist/lib/storage').default;
    
        const persistConfig = {
            key: 'root',
            storage
        };
        store = createStore(
            persistReducer(persistConfig, combineReducers({
                formState : FormStateReducer,
                products: ProductsReducer,
                shipping: ShippingReducer,
                hearts: LikedProductsReducer
            })),
            composeWithDevTools(
                applyMiddleware(thunk)
            )
            
        );
    
        store.__PERSISTOR = persistStore(store);




    } else {
        store = createStore(
            combineReducers({
                formState : FormStateReducer,
                products: ProductsReducer,
                shipping: ShippingReducer,
                hearts: LikedProductsReducer
            }),
            composeWithDevTools(
                applyMiddleware(thunk)
            )
        )
    }

    return store;
}



export default configureStore;
export const wrapper = createWrapper(configureStore);